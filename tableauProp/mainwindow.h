#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

namespace ogdf {
class NodeElement;
using node = NodeElement*;
class Graph;
class GraphAttributes;
class SugiyamaLayout;
}

class SvgView;
class QGraphicsScene;
class BaseFormula;
class BinaryConnective;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void loadFormula();

private:
    Ui::MainWindow *ui;
    bool loadFile(const QString &fileName);
    void clearAll();

    void saveAs();

    QString genErrorMessage(QString msg) const;

    bool tableauProcedure();
    void getLeavesOfSubtree(ogdf::node subtreeRoot,
                            std::vector<ogdf::node>& subtreeLeaves);

    void addNodes(std::vector<ogdf::node>& nodeLeaves,
                  BinaryConnective* bin);
    bool isPathClosed(ogdf::node startNode);

    ogdf::node createNewNode(BaseFormula* f, std::string fString = "");

    void createHierarchicalLayout(ogdf::SugiyamaLayout &SL);

    ogdf::Graph *m_graph{nullptr};
    ogdf::GraphAttributes* m_graphAttr{nullptr};
    std::vector<ogdf::node> m_nodesList;
    std::map<ogdf::node, BaseFormula*> m_nodeFormula;
};

#endif // MAINWINDOW_H
