#include "baseformula.h"
#include <tuple>
#include <sstream>

BaseFormula* BaseFormula::top_node = nullptr;

BaseFormula::BaseFormula()
{
    top_node = this;
}

BaseFormula::~BaseFormula()
{

}

std::string BaseFormula::toString() const
{
    std::stringstream ss;
    print(ss);
    return ss.str();
}

Variable::Variable(char c)
    : m_c(c)
{

}

BaseFormula *Variable::nnf()
{
    return this;
}

std::pair<BaseFormula*, BaseFormula*> Variable::getOperands() const
{
    return std::pair<BaseFormula*, BaseFormula*>(nullptr, nullptr);
}

bool Variable::isOpposite(BaseFormula *f) const
{
    if (dynamic_cast<Not*>(f)) {
        BaseFormula *op1, *op2;
        std::tie(op1, op2) = f->getOperands(); // ignore op2
        Variable *f_var = dynamic_cast<Variable*>(op1);
        if (f_var && f_var->getName() == getName())
                return true;
    }
    return false;
}

char Variable::getName() const
{
    return m_c;
}

std::ostream &Variable::print(std::ostream &out) const
{
    return out << m_c;
}

BinaryConnective::BinaryConnective(BaseFormula *op1, BaseFormula *op2)
    : BaseFormula(), m_op1(op1), m_op2(op2)
{

}

std::pair<BaseFormula*, BaseFormula*> BinaryConnective::getOperands() const
{
    return std::pair<BaseFormula*, BaseFormula*>(m_op1, m_op2);
}

bool BinaryConnective::isOpposite(BaseFormula *f) const
{
    (void)f;
    return false;
}

std::ostream& BinaryConnective::print(std::ostream &out) const
{
    out << "( ";
    m_op1->print(out);
    out << " " << symbol() << " ";
    m_op2->print(out);
    return out << " )";
}


And::And(BaseFormula *op1, BaseFormula *op2)
    : BinaryConnective(op1, op2)
{

}

std::string And::symbol() const
{
    return "/\\";
}

BaseFormula *And::nnf()
{
    return new And(m_op1->nnf(), m_op2->nnf());
}

Or::Or(BaseFormula *op1, BaseFormula *op2)
    : BinaryConnective(op1, op2)
{

}

std::string Or::symbol() const
{
    return "\\/";
}

BaseFormula *Or::nnf()
{
    return new Or(m_op1->nnf(), m_op2->nnf());
}

Impl::Impl(BaseFormula *op1, BaseFormula *op2)
    : BinaryConnective(op1, op2)
{

}

std::string Impl::symbol() const
{
    return "=>";
}

BaseFormula *Impl::nnf()
{
    return new Or(new Not(m_op1->nnf()), m_op2->nnf());
}

Equiv::Equiv(BaseFormula *op1, BaseFormula *op2)
    : BinaryConnective(op1, op2)
{

}

std::string Equiv::symbol() const
{
    return "<=>";
}

BaseFormula *Equiv::nnf()
{
    BaseFormula *nnfOp1 = m_op1->nnf();
    BaseFormula *nnfOp2 = m_op2->nnf();
    return new And(new Or(new Not(nnfOp1), nnfOp2),
                   new Or(new Not(nnfOp2), nnfOp1));
}

Not::Not(BaseFormula *op)
    : BaseFormula(), m_op(op)
{

}

std::pair<BaseFormula*, BaseFormula*> Not::getOperands() const
{
    return std::pair<BaseFormula*, BaseFormula*>(m_op, nullptr);
}

std::ostream &Not::print(std::ostream &out) const
{
//    out << "~( ";
    out << "~"; // if in NNF, print without parethesis is ok
    m_op->print(out);
//    return out << " )";
    return out;
}

BaseFormula *Not::nnf()
{
    BaseFormula *nnfOp = m_op->nnf();
    if (dynamic_cast<And*>(nnfOp)) { // ~(p /\ q)
        BaseFormula *op1, *op2;
        std::tie(op1, op2) = nnfOp->getOperands();
        BaseFormula *not1 = new Not(op1);
        BaseFormula *not2 = new Not(op2);
        return new Or(not1->nnf(), not2->nnf());
    }
    else if (dynamic_cast<Or*>(nnfOp)) { // ~(p \/ q)
        BaseFormula *op1, *op2;
        std::tie(op1, op2) = nnfOp->getOperands();
        BaseFormula *not1 = new Not(op1);
        BaseFormula *not2 = new Not(op2);
        return new And(not1->nnf(), not2->nnf());
    }
    else if (dynamic_cast<Impl*>(nnfOp)) { // ~(p => q) -> p /\ ~q
        BaseFormula *op1, *op2;
        std::tie(op1, op2) = nnfOp->getOperands();
        BaseFormula *not2 = new Not(op2);
        return new And(op1->nnf(), not2->nnf());
    }
    else if (dynamic_cast<Equiv*>(nnfOp)) { // ~(p <=> q) -> (p /\ ~q) \/ (~p /\ q)
        BaseFormula *op1, *op2;
        std::tie(op1, op2) = nnfOp->getOperands();
        BaseFormula *not1 = new Not(op1);
        BaseFormula *not2 = new Not(op2);
        return new Or(new And(op1, not1->nnf()),
                      new And(not2->nnf(), op2));
    }
    else if (dynamic_cast<Not*>(nnfOp)) { // ~(~(p)) -> p
        BaseFormula *op1, *op2;
        std::tie(op1, op2) = nnfOp->getOperands();
        return op1->nnf();
    }
    else if (dynamic_cast<Variable*>(nnfOp)) {
        return this;
    }
    else {
        throw std::runtime_error("uknown operand");
    }
}

bool Not::isOpposite(BaseFormula *f) const
{
    Variable *fVar = dynamic_cast<Variable*>(f);
    if (fVar) {
        Variable *opVar = dynamic_cast<Variable*>(m_op);
        if (opVar && opVar->getName() == fVar->getName())
            return true;
    }
    return false;
}
