#ifndef CLICKABLENODEITEM_H
#define CLICKABLENODEITEM_H

#include <QObject>
#include <QGraphicsPolygonItem>

namespace ogdf {
class NodeElement;
using node = NodeElement*;

class GraphAttributes;
}

/**
 * @brief The ClickableNodeItem class
 * Wrapper around ogdf::node, essentially, it is clickable rectangle-frame around it.
 */
class ClickableNodeItem : public QGraphicsPolygonItem
{
public:
    ClickableNodeItem(ogdf::node node, ogdf::GraphAttributes* ga);

private:
    ogdf::node m_node;
    ogdf::GraphAttributes* m_ga;

    // QGraphicsItem interface
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // CLICKABLENODEITEM_H
