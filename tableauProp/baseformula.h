#ifndef BASEFORMULA_H
#define BASEFORMULA_H

#include <vector>
#include <string>
#include <iostream>
#include <QChar>

class BaseFormula;

class BaseFormula
{
public:
    BaseFormula();
    virtual BaseFormula *nnf() = 0;
    virtual std::pair<BaseFormula*, BaseFormula*> getOperands() const = 0;
    virtual bool isOpposite(BaseFormula *f) const = 0;
    virtual std::ostream& print(std::ostream &out) const = 0;
    virtual ~BaseFormula();
    std::string toString() const;
    static BaseFormula* top_node;
private:
};

class Variable : public BaseFormula
{
public:
    Variable(char c);
    BaseFormula *nnf();
    std::pair<BaseFormula*, BaseFormula*> getOperands() const;
    bool isOpposite(BaseFormula *f) const;
    char getName() const;
    std::ostream& print(std::ostream &out) const;
private:
    char m_c;
};

class BinaryConnective : public BaseFormula
{
public:
    BinaryConnective(BaseFormula *op1, BaseFormula *op2);
    std::pair<BaseFormula*, BaseFormula*> getOperands() const;
    bool isOpposite(BaseFormula *f) const;
    virtual std::string symbol() const = 0;
    std::ostream& print(std::ostream &out) const;
protected:
    BaseFormula *m_op1;
    BaseFormula *m_op2;
};

class And : public BinaryConnective
{
public:
    And(BaseFormula *op1, BaseFormula *op2);
    std::string symbol() const;
    BaseFormula *nnf();
};

class Or : public BinaryConnective
{
public:
    Or(BaseFormula *op1, BaseFormula *op2);
    std::string symbol() const;
    BaseFormula *nnf();
};

class Impl : public BinaryConnective
{
public:
    Impl(BaseFormula *op1, BaseFormula *op2);
    std::string symbol() const;
    BaseFormula *nnf();
};

class Equiv : public BinaryConnective
{
public:
    Equiv(BaseFormula *op1, BaseFormula *op2);
    std::string symbol() const;
    BaseFormula *nnf();
};

class Not : public BaseFormula
{
public:
    Not(BaseFormula *op);
    BaseFormula *nnf();
    std::pair<BaseFormula*, BaseFormula*> getOperands() const;
    bool isOpposite(BaseFormula *f) const;
    std::ostream& print(std::ostream &out) const;
private:
    BaseFormula *m_op;
};

#endif // BASEFORMULA_H
