#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <queue>

#include <QDir>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QFontMetrics>
#include <QDebug>

#include <ogdf/fileformats/GraphIO.h>
#include <ogdf/layered/SugiyamaLayout.h>
#include <ogdf/layered/OptimalRanking.h>
#include <ogdf/layered/MedianHeuristic.h>
#include <ogdf/layered/OptimalHierarchyLayout.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/layered/OptimalHierarchyLayout.h>

#include <ogdf/planarity/PlanarizationLayout.h>

#include "clickablenodeitem.h"
#include "baseformula.h"
#include "lexer.h"
#include "parser.h" // MUST BE AT THE END OF INCLUDE BLOCK

#define LOCATION (QString(__FILE__) + ":" + QString(__LINE__))

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->labelConclusion->setVisible(false);

    connect(ui->pushButtonTableau, &QPushButton::clicked,
            this, &MainWindow::loadFormula);
    connect(ui->lineEdit, &QLineEdit::textEdited,
            this, [this] () { ui->labelConclusion->setVisible(false); });
    connect(ui->pushButtonPrint, &QPushButton::clicked,
            this, &MainWindow::saveAs);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadFormula()
{
    clearAll();

    QString formula = ui->lineEdit->text();
    YY_BUFFER_STATE bufferState = yy_scan_string(formula.toUtf8().constData());
    int syntaxError = yyparse();
    yy_delete_buffer(bufferState);

    if (!syntaxError) {
        ui->lineEdit->setStyleSheet("");

        Not* negateF = new Not(BaseFormula::top_node);
        negateF->nnf();

        BaseFormula::top_node->print(std::cout);
        std::flush(std::cout);

        bool isTautology = tableauProcedure();
        qDebug() << "tautology? " << isTautology;

        QString richText = "<html><head/><body><p><span style=\" ";
        richText.append("font-size:16pt; font-style:italic; color:");
        if (isTautology)
            richText.append("green;\">Tautology!");
        else
            richText.append("red;\">Not tautology!");
        richText.append("</span></p></body></html>");
        ui->labelConclusion->setText(richText);
        ui->labelConclusion->setVisible(true);

        loadFile("output_graph.svg");
    }
    else {
        ui->lineEdit->setStyleSheet("border: 1px solid red");
    }
}

bool MainWindow::loadFile(const QString &fileName)
{
    if (!QFileInfo::exists(fileName) || !ui->svgViewer->openFile(fileName)) {
        QString errMsg = "Internal error. Please report to the developer.";
        QMessageBox::critical(this, "Open SVG File", errMsg);
        return false;
    }

    setWindowFilePath(fileName);

    QString fInfo = QFileInfo(fileName).absoluteFilePath();
    const QSize size = ui->svgViewer->svgSize();
    fInfo.append(tr(", %1x%2").arg(size.width()).arg(size.width()));
    statusBar()->showMessage("Picture generated in " + fInfo);

    return true;
}

void MainWindow::clearAll()
{
    delete m_graph;
    delete m_graphAttr;
    m_nodesList.clear();
    m_nodeFormula.clear();
}

void MainWindow::saveAs()
{
    QString fileNameFrom("output_graph.svg");
    if (!QFileInfo::exists(fileNameFrom)) {
        QMessageBox::critical(this, "Open SVG File", genErrorMessage(LOCATION));
        return;
    }
    QString fileNameTo = QFileDialog::getSaveFileName(
                this, tr("Save Proof As"), QDir::homePath(),
                tr("Scalable Vector Graphics (*.svg)"));
    if (!fileNameTo.endsWith(".svg"))
        fileNameTo.append(".svg");

    QFile from(fileNameFrom);
    QFile target(fileNameTo);
    if (target.exists())
        target.remove();
    if (target.error() != QFile::NoError)
        qDebug() << genErrorMessage(LOCATION);
    from.copy(target.fileName());
    if (from.error() != QFile::NoError)
        qDebug() << genErrorMessage(LOCATION);
}

QString MainWindow::genErrorMessage(QString msg) const
{
    QString errMsg = "Internal error. Please report to the developer.\n";
    return errMsg + msg;
}

bool MainWindow::tableauProcedure()
{
    m_graph = new ogdf::Graph;
    long attr = ogdf::GraphAttributes::nodeGraphics |
            ogdf::GraphAttributes::edgeGraphics |
            ogdf::GraphAttributes::nodeTemplate |
            ogdf::GraphAttributes::nodeStyle |
            ogdf::GraphAttributes::edgeStyle |
            ogdf::GraphAttributes::nodeLabel |
            ogdf::GraphAttributes::edgeLabel;
    m_graphAttr = new ogdf::GraphAttributes(*m_graph, attr);
    createNewNode(BaseFormula::top_node);

    bool isTreeClosed = false;

    for (unsigned i = 0; i < m_nodesList.size(); i++) {
        ogdf::node currNode = m_nodesList[i];
        std::vector<ogdf::node> currNodeLeaves;
        getLeavesOfSubtree(currNode, currNodeLeaves);
        unsigned closedPathsNum = 0;

        qDebug() << QString::fromStdString(m_nodeFormula[currNode]->toString()) << "  <-- node to decompose";

        BinaryConnective* binFormula = dynamic_cast<BinaryConnective*>(m_nodeFormula[currNode]);
        if (binFormula) { // since this formula is in NNF, operators could only be AND or OR
            addNodes(currNodeLeaves, binFormula);
        }

        getLeavesOfSubtree(m_nodesList[0], currNodeLeaves);
        for (const auto& leafNode : currNodeLeaves) {
            // check every leaf node (or its direct parent), if it introduces contradiction
            if (isPathClosed(leafNode)) {
                closedPathsNum++; // path is closed
            }
        }
        if (closedPathsNum > 0 && (closedPathsNum == currNodeLeaves.size())) {
            isTreeClosed = true;
            break; // there is no need to continue with procedure
        }
    }

    for (const auto& currNode : m_nodesList) {
        m_graphAttr->strokeType(currNode) = ogdf::StrokeType::None;

        QFontMetrics fm = QApplication::fontMetrics();
        std::string label = m_graphAttr->label(currNode);
        int labelWidth = fm.width(QString::fromStdString(label));
        m_graphAttr->width(currNode) = labelWidth;
    }

    ogdf::SugiyamaLayout SL;
    createHierarchicalLayout(SL);
    m_graphAttr->setAllHeight(40);
    SL.call(*m_graphAttr);

    QGraphicsScene* graphicsScene = new QGraphicsScene(ui->svgViewer);
    ui->svgViewer->setScene(graphicsScene);
    // Now generate svg of tableau tree
    ogdf::GraphIO::write(*m_graphAttr, "output_graph.svg", ogdf::GraphIO::drawSVG);

    ClickableNodeItem* clickableNode;
    for(const auto& currNode : m_nodesList) {
        clickableNode = new ClickableNodeItem(currNode, m_graphAttr);
        graphicsScene->addItem(clickableNode);
    }

    return isTreeClosed;
}

void MainWindow::getLeavesOfSubtree(ogdf::node subtreeRoot,
                                    std::vector<ogdf::node>& subtreeLeaves)
{
    subtreeLeaves.clear();

    std::queue<ogdf::node> Q;
    Q.push(subtreeRoot);
    while (!Q.empty()) {
        ogdf::node currNode = Q.front();
        Q.pop();

        ogdf::List<ogdf::edge> outEdges;
        currNode->outEdges(outEdges);

        if (outEdges.empty())
            subtreeLeaves.push_back(currNode);

        for (const auto& edge : outEdges)
            Q.push(edge->target());
    }
}

/**
 * @brief MainWindow::isPathClosed
 * @param startNode
 * Checks if startNode (or its parent) has contradiction on the path to the root.
 */
bool MainWindow::isPathClosed(ogdf::node startNode)
{
    ogdf::List<ogdf::edge> inEdges;
    startNode->inEdges(inEdges);
    if (inEdges.empty())
        return false; // this must be the root node

    ogdf::node parentNode = inEdges.front()->source();

    auto isPathClosed_ = [this] (ogdf::node testNode) {
        ogdf::node currNode = testNode;
        do {
            ogdf::List<ogdf::edge> inEdges;
            currNode->inEdges(inEdges);
            if (inEdges.empty())
                return false; // this must be the root node
            ogdf::node parentOfCurrNode = inEdges.front()->source();

            // check whether on the path to the root, there is a contradicting node
            BaseFormula* currFormula = m_nodeFormula[testNode];
            if (currFormula->isOpposite(m_nodeFormula[parentOfCurrNode])) {
                m_graphAttr->fillColor(testNode) = ogdf::Color::Name::Crimson;
                // TODO: add parentOfCurrNode to contradiction map for key=testNode
                // then later from clickable node, when it is clicked, paint that node also
                return true;
            }

            currNode = parentOfCurrNode;
        } while (true);
    };

    if (isPathClosed_(startNode) || isPathClosed_(parentNode))
        return true;
    return false;
}

ogdf::node MainWindow::createNewNode(BaseFormula *f, std::string fString)
{
    ogdf::node newNode = m_graph->newNode();
    m_nodesList.push_back(newNode);

    std::string formulaString = (fString.empty()) ? f->toString() : fString;
    if ((formulaString.front() == '(') && (formulaString.size() > 1)) // trim parenthesis
        formulaString = formulaString.substr(1, formulaString.size() - 2);

    m_graphAttr->label(newNode) = formulaString;

    m_nodeFormula[newNode] = f;
    return newNode;
}

void MainWindow::addNodes(std::vector<ogdf::node>& nodeLeaves, BinaryConnective *bin)
{
    BaseFormula *op1, *op2;
    std::tie(op1, op2) = bin->getOperands();
    std::string op1Label = op1->toString();
    std::string op2Label = op2->toString();

    bool isConjunction = dynamic_cast<And*>(bin);

    for ( const auto& nodeLeaf : nodeLeaves) {

        qDebug() << QString::fromStdString(m_nodeFormula[nodeLeaf]->toString()) << "  <-- leaf node";

        if (m_graphAttr->fillColor(nodeLeaf) == ogdf::Color::Name::Crimson)
            continue; // skip leaf nodes that are dead ends

        ogdf::node op1Node = createNewNode(op1, op1Label);
        ogdf::node op2Node = createNewNode(op2, op2Label);

        m_graph->newEdge(nodeLeaf, op1Node);
        if (isConjunction) { // add 2 connected edges: x---op1---op2
            m_graph->newEdge(op1Node, op2Node);
        }
        else { //                                     ___op1
            // do the branching in the tableau tree x/___op2
            m_graph->newEdge(nodeLeaf, op2Node);
        }
    }
}

void MainWindow::createHierarchicalLayout(ogdf::SugiyamaLayout& SL)
{
    SL.setRanking(new ogdf::OptimalRanking);
    SL.setCrossMin(new ogdf::MedianHeuristic);

    ogdf::OptimalHierarchyLayout *ohl = new ogdf::OptimalHierarchyLayout;
    ohl->layerDistance(30.0);
    ohl->nodeDistance(25.0);
    ohl->weightBalancing(0.8);
    SL.setLayout(ohl);
}
