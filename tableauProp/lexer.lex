%option noyywrap
%option nounput
%{
#include <iostream>
#include <cstdlib>

#include "baseformula.h"
#include "parser.h"

%}



%%
[a-z]           { yylval.var = yytext[0];
                  return var_token; }
"/\\"           return and_token;
"\\/"           return or_token;
"=>"            return impl_token;
"<=>"           return eq_token;
[()~]           return *yytext;

[ \t\n]   {  }
.   {
    std::cerr << "\nlexical error - unsupported character: "
              << *yytext;
}
%%


