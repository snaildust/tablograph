#include "clickablenodeitem.h"

#include <QBrush>
#include <QVector>
#include <QPointF>
#include <QDebug>

#include <ogdf/basic/Graph_d.h>
#include <ogdf/basic/GraphAttributes.h>
#include <QGraphicsSceneMouseEvent>

ClickableNodeItem::ClickableNodeItem(ogdf::node node, ogdf::GraphAttributes* ga)
    : m_node(node), m_ga(ga)
{
    double w = ga->width(node);
    double h = ga->height(node);
    // Adjust position to fit margins
    QRectF nodeSh(-w/2 - 28.5, -h - 8.5, w, h);
    QPolygonF nodeShape(nodeSh);
    setPolygon(nodeShape);

    setFlags(flags() |
             QGraphicsItem::ItemIsSelectable |
             QGraphicsItem::ItemSendsGeometryChanges);
    setZValue(10000);

    setBrush(QColor(1, 1, 0, 0));

    setPos(ga->x(node), ga->y(node));
}

void ClickableNodeItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(event->button() == Qt::LeftButton) {
        std::string localLabel = m_ga->label(m_node);
        qDebug() << "Node " << QString::fromStdString(localLabel) << "has been clicked";
    }
}
