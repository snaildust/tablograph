Demonstracija metoda tabloa za iskaznu logiku.

Rad ove C++/Qt aplikacije se oslanja na OGDF biblioteku za iscrtavanje grafova.

Uputstvo kako instalirati OGDF bilbioteku se nalazi na linku https://github.com/ogdf/ogdf/blob/master/doc/build.md#default-configuration.

Nakon ispracenih koraka iz te sekcije, dovoljno je (iz istog direktorijuma) pozvati `sudo make install`.


Aplikacija je testirana na:

- Qt verzijama 5.11.0 i 5.12.1

- komitu [8a103cf3a7df](https://github.com/ogdf/ogdf/commit/8a103cf3a7dfff87fe8b7534575604bc53c0870c) OGDF repozitorijuma

![tool in use](tool-in-use.png)



Tablo dokaz se u vidu vektorske grafike moze eksportovati koriscenjem dugmeta "Print" (primer: [output_graph.svg](output_graph.svg)).
